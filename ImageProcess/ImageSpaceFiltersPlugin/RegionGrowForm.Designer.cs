﻿namespace ImageSpaceFiltersPlugin
{
    partial class RegionGrowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegionGrowForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.inverseSelectionButton = new System.Windows.Forms.Button();
            this.growRegionButton = new System.Windows.Forms.Button();
            this.undoButton = new System.Windows.Forms.Button();
            this.similarityUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.colorPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.moveToolButton = new System.Windows.Forms.ToolStripButton();
            this.plantSeedToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomIn = new System.Windows.Forms.ToolStripButton();
            this.zoomOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.undoToolButton = new System.Windows.Forms.ToolStripButton();
            this.imageView = new Common.View();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.similarityUpDown)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.inverseSelectionButton);
            this.panel1.Controls.Add(this.growRegionButton);
            this.panel1.Controls.Add(this.undoButton);
            this.panel1.Controls.Add(this.similarityUpDown);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(556, 41);
            this.panel1.TabIndex = 0;
            // 
            // inverseSelectionButton
            // 
            this.inverseSelectionButton.Location = new System.Drawing.Point(377, 8);
            this.inverseSelectionButton.Name = "inverseSelectionButton";
            this.inverseSelectionButton.Size = new System.Drawing.Size(110, 23);
            this.inverseSelectionButton.TabIndex = 6;
            this.inverseSelectionButton.Text = "&Inverse Selection";
            this.inverseSelectionButton.UseVisualStyleBackColor = true;
            this.inverseSelectionButton.Click += new System.EventHandler(this.inverseSelectionButton_Click);
            // 
            // growRegionButton
            // 
            this.growRegionButton.Location = new System.Drawing.Point(278, 8);
            this.growRegionButton.Name = "growRegionButton";
            this.growRegionButton.Size = new System.Drawing.Size(93, 23);
            this.growRegionButton.TabIndex = 5;
            this.growRegionButton.Text = "&Grow Region";
            this.growRegionButton.UseVisualStyleBackColor = true;
            this.growRegionButton.Click += new System.EventHandler(this.growRegionButton_Click);
            // 
            // undoButton
            // 
            this.undoButton.Location = new System.Drawing.Point(209, 8);
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(63, 23);
            this.undoButton.TabIndex = 4;
            this.undoButton.Text = "&Undo";
            this.undoButton.UseVisualStyleBackColor = true;
            this.undoButton.Click += new System.EventHandler(this.undoButton_Click);
            // 
            // similarityUpDown
            // 
            this.similarityUpDown.Location = new System.Drawing.Point(137, 11);
            this.similarityUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.similarityUpDown.Name = "similarityUpDown";
            this.similarityUpDown.Size = new System.Drawing.Size(53, 20);
            this.similarityUpDown.TabIndex = 1;
            this.similarityUpDown.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Similarity Threashold:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 374);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(556, 36);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.colorPanel);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cancelButton);
            this.panel3.Controls.Add(this.okButton);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(186, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(370, 36);
            this.panel3.TabIndex = 0;
            // 
            // colorPanel
            // 
            this.colorPanel.BackColor = System.Drawing.Color.Fuchsia;
            this.colorPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.colorPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.colorPanel.Location = new System.Drawing.Point(107, 6);
            this.colorPanel.Name = "colorPanel";
            this.colorPanel.Size = new System.Drawing.Size(66, 21);
            this.colorPanel.TabIndex = 2;
            this.colorPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.colorPanel_Paint);
            this.colorPanel.Click += new System.EventHandler(this.colorPanel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-2, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Background Color:";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(283, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(201, 4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveToolButton,
            this.plantSeedToolButton,
            this.toolStripSeparator1,
            this.zoomIn,
            this.zoomOut,
            this.toolStripSeparator2,
            this.undoToolButton});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 41);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(31, 333);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // moveToolButton
            // 
            this.moveToolButton.Checked = true;
            this.moveToolButton.CheckOnClick = true;
            this.moveToolButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.moveToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.moveToolButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.moveToolButton.Image = ((System.Drawing.Image)(resources.GetObject("moveToolButton.Image")));
            this.moveToolButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.moveToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.moveToolButton.Name = "moveToolButton";
            this.moveToolButton.Size = new System.Drawing.Size(28, 30);
            this.moveToolButton.ToolTipText = "Move Tool";
            this.moveToolButton.Click += new System.EventHandler(this.moveToolButton_Click);
            // 
            // plantSeedToolButton
            // 
            this.plantSeedToolButton.CheckOnClick = true;
            this.plantSeedToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.plantSeedToolButton.Image = ((System.Drawing.Image)(resources.GetObject("plantSeedToolButton.Image")));
            this.plantSeedToolButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.plantSeedToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.plantSeedToolButton.Name = "plantSeedToolButton";
            this.plantSeedToolButton.Size = new System.Drawing.Size(28, 30);
            this.plantSeedToolButton.ToolTipText = "Plant Seeds Tool";
            this.plantSeedToolButton.Click += new System.EventHandler(this.plantSeedToolButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(28, 6);
            // 
            // zoomIn
            // 
            this.zoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomIn.Image = ((System.Drawing.Image)(resources.GetObject("zoomIn.Image")));
            this.zoomIn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.zoomIn.ImageTransparentColor = System.Drawing.Color.White;
            this.zoomIn.Name = "zoomIn";
            this.zoomIn.Size = new System.Drawing.Size(28, 29);
            this.zoomIn.Text = "toolStripButton1";
            this.zoomIn.ToolTipText = "ZoomIn";
            this.zoomIn.Click += new System.EventHandler(this.zoomIn_Click);
            // 
            // zoomOut
            // 
            this.zoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOut.Image = ((System.Drawing.Image)(resources.GetObject("zoomOut.Image")));
            this.zoomOut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.zoomOut.ImageTransparentColor = System.Drawing.Color.White;
            this.zoomOut.Name = "zoomOut";
            this.zoomOut.Size = new System.Drawing.Size(28, 29);
            this.zoomOut.Text = "toolStripButton2";
            this.zoomOut.ToolTipText = "Zoom Out";
            this.zoomOut.Click += new System.EventHandler(this.zoomOut_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(28, 6);
            // 
            // undoToolButton
            // 
            this.undoToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolButton.Image = ((System.Drawing.Image)(resources.GetObject("undoToolButton.Image")));
            this.undoToolButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.undoToolButton.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.undoToolButton.Name = "undoToolButton";
            this.undoToolButton.Size = new System.Drawing.Size(28, 20);
            this.undoToolButton.Click += new System.EventHandler(this.undoButton_Click);
            // 
            // imageView
            // 
            this.imageView.BackColor = System.Drawing.SystemColors.Window;
            this.imageView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageView.EnableMoving = true;
            this.imageView.Location = new System.Drawing.Point(31, 41);
            this.imageView.Name = "imageView";
            this.imageView.Size = new System.Drawing.Size(525, 333);
            this.imageView.TabIndex = 3;
            this.imageView.ViewMode = Common.ViewMode.CustomZoomFactor;
            this.imageView.ZoomFactor = 20;
            this.imageView.ImageMouseDown += new Common.View.ImageMouseEvent(this.imageView_ImageMouseDown);
            this.imageView.DrawOverlay += new Common.View.DrawOverlayDelegate(this.imageView_DrawOverlay);
            // 
            // colorDialog
            // 
            this.colorDialog.Color = System.Drawing.Color.Fuchsia;
            // 
            // RegionGrowForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(556, 410);
            this.Controls.Add(this.imageView);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 290);
            this.Name = "RegionGrowForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Region Growing Segementation";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.similarityUpDown)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button undoButton;
        private System.Windows.Forms.NumericUpDown similarityUpDown;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton moveToolButton;
        private System.Windows.Forms.ToolStripButton plantSeedToolButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton undoToolButton;
        private Common.View imageView;
        private System.Windows.Forms.ToolStripButton zoomIn;
        private System.Windows.Forms.ToolStripButton zoomOut;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Button growRegionButton;
        private System.Windows.Forms.Panel colorPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button inverseSelectionButton;
    }
}
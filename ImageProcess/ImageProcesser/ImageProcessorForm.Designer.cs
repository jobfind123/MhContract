﻿namespace ImageProcesser
{
    partial class ImageProcessorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageProcessorForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.bestFitButton = new System.Windows.Forms.ToolStripButton();
            this.originalSizeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnUp = new System.Windows.Forms.ToolStripButton();
            this.btnDown = new System.Windows.Forms.ToolStripButton();
            this.bottomRightPanel = new System.Windows.Forms.Panel();
            this.zoomRateLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.zoomFactorTrackBar = new Common.CompactTrackBar();
            this.docPanel = new System.Windows.Forms.Panel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainMenu.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.bottomRightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomFactorTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editMenu,
            this.transformToolStripMenuItem,
            this.filterToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.mainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mainMenu.Size = new System.Drawing.Size(937, 28);
            this.mainMenu.TabIndex = 0;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::ImageProcesser.Properties.Resources.OpenFolder;
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Image = global::ImageProcesser.Properties.Resources.Save;
            this.saveAsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
            this.saveAsToolStripMenuItem.Text = "&Save";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(173, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(49, 24);
            this.editMenu.Text = "&Edit";
            this.editMenu.DropDownClosed += new System.EventHandler(this.editMenu_DropDownClosed);
            this.editMenu.DropDownOpening += new System.EventHandler(this.editMenu_DropDownOpening);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Image = global::ImageProcesser.Properties.Resources.Edit_Undo;
            this.undoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(173, 24);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Image = global::ImageProcesser.Properties.Resources.Edit_Redo;
            this.redoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(173, 24);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // transformToolStripMenuItem
            // 
            this.transformToolStripMenuItem.Name = "transformToolStripMenuItem";
            this.transformToolStripMenuItem.Size = new System.Drawing.Size(95, 24);
            this.transformToolStripMenuItem.Text = "&Transform";
            this.transformToolStripMenuItem.DropDownClosed += new System.EventHandler(this.transformToolStripMenuItem_DropDownClosed);
            this.transformToolStripMenuItem.DropDownOpening += new System.EventHandler(this.filterToolStripMenuItem_DropDownOpening);
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.filterToolStripMenuItem.Text = "F&ilter";
            this.filterToolStripMenuItem.DropDownClosed += new System.EventHandler(this.transformToolStripMenuItem_DropDownClosed);
            this.filterToolStripMenuItem.DropDownOpening += new System.EventHandler(this.filterToolStripMenuItem_DropDownOpening);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(124, 24);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.toolStrip1);
            this.bottomPanel.Controls.Add(this.bottomRightPanel);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 539);
            this.bottomPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Padding = new System.Windows.Forms.Padding(5);
            this.bottomPanel.Size = new System.Drawing.Size(937, 37);
            this.bottomPanel.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bestFitButton,
            this.originalSizeButton,
            this.toolStripSeparator1,
            this.btnUp,
            this.btnDown});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(407, 5);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(101, 27);
            this.toolStrip1.TabIndex = 2;
            // 
            // bestFitButton
            // 
            this.bestFitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bestFitButton.Image = ((System.Drawing.Image)(resources.GetObject("bestFitButton.Image")));
            this.bestFitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bestFitButton.Name = "bestFitButton";
            this.bestFitButton.Size = new System.Drawing.Size(23, 24);
            this.bestFitButton.Text = "刚刚好";
            this.bestFitButton.ToolTipText = "刚刚好";
            this.bestFitButton.Click += new System.EventHandler(this.bestFitButton_Click);
            // 
            // originalSizeButton
            // 
            this.originalSizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.originalSizeButton.Image = ((System.Drawing.Image)(resources.GetObject("originalSizeButton.Image")));
            this.originalSizeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.originalSizeButton.Name = "originalSizeButton";
            this.originalSizeButton.Size = new System.Drawing.Size(23, 24);
            this.originalSizeButton.Text = "原始大小";
            this.originalSizeButton.ToolTipText = "原始大小";
            this.originalSizeButton.Click += new System.EventHandler(this.originalSizeButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // btnUp
            // 
            this.btnUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUp.Image = global::ImageProcesser.Properties.Resources.GoToPreviousRecord;
            this.btnUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(23, 24);
            this.btnUp.Text = "上一页";
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDown.Image = global::ImageProcesser.Properties.Resources.GoToNextRecord;
            this.btnDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(23, 24);
            this.btnDown.Text = "下一页";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // bottomRightPanel
            // 
            this.bottomRightPanel.Controls.Add(this.zoomRateLabel);
            this.bottomRightPanel.Controls.Add(this.label1);
            this.bottomRightPanel.Controls.Add(this.zoomFactorTrackBar);
            this.bottomRightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.bottomRightPanel.Location = new System.Drawing.Point(508, 5);
            this.bottomRightPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.bottomRightPanel.Name = "bottomRightPanel";
            this.bottomRightPanel.Size = new System.Drawing.Size(424, 27);
            this.bottomRightPanel.TabIndex = 1;
            // 
            // zoomRateLabel
            // 
            this.zoomRateLabel.AutoSize = true;
            this.zoomRateLabel.Location = new System.Drawing.Point(359, 5);
            this.zoomRateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.zoomRateLabel.Name = "zoomRateLabel";
            this.zoomRateLabel.Size = new System.Drawing.Size(39, 15);
            this.zoomRateLabel.TabIndex = 2;
            this.zoomRateLabel.Text = "100%";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "缩放:";
            // 
            // zoomFactorTrackBar
            // 
            this.zoomFactorTrackBar.Location = new System.Drawing.Point(71, 0);
            this.zoomFactorTrackBar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.zoomFactorTrackBar.Maximum = 20;
            this.zoomFactorTrackBar.Minimum = 1;
            this.zoomFactorTrackBar.Name = "zoomFactorTrackBar";
            this.zoomFactorTrackBar.Size = new System.Drawing.Size(287, 56);
            this.zoomFactorTrackBar.TabIndex = 0;
            this.zoomFactorTrackBar.TabStop = false;
            this.zoomFactorTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.zoomFactorTrackBar.Value = 10;
            // 
            // docPanel
            // 
            this.docPanel.AllowDrop = true;
            this.docPanel.BackColor = System.Drawing.SystemColors.Window;
            this.docPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.docPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.docPanel.Location = new System.Drawing.Point(0, 28);
            this.docPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.docPanel.Name = "docPanel";
            this.docPanel.Size = new System.Drawing.Size(937, 511);
            this.docPanel.TabIndex = 2;
            this.docPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.docPanel_DragDrop);
            this.docPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.docPanel_DragEnter);
            this.docPanel.DragOver += new System.Windows.Forms.DragEventHandler(this.docPanel_DragOver);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "bmp";
            this.openFileDialog.Filter = "All Supported Formats|*.gif;*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff|Bitmaps|*.bmp|J" +
    "PEG Images|*.jpg;*.jpeg|PNG Images|*.png|GIF Images|*.gif|TIF Images|*.tif;*.tif" +
    "f";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "bmp";
            this.saveFileDialog.Filter = "All Supported Formats|*.gif;*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff|Bitmaps|*.bmp|J" +
    "PEG Images|*.jpg;*.jpeg|PNG Images|*.png|GIF Images|*.gif|TIF Images|*.tif;*.tif" +
    "f";
            // 
            // ImageProcessorForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 576);
            this.Controls.Add(this.docPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.mainMenu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImageProcessorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "合同扫描件浏览";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ImageProcessorForm_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.bottomRightPanel.ResumeLayout(false);
            this.bottomRightPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomFactorTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transformToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Panel bottomPanel;
        private Common.CompactTrackBar zoomFactorTrackBar;
        private System.Windows.Forms.Panel bottomRightPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label zoomRateLabel;
        private System.Windows.Forms.Panel docPanel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton bestFitButton;
        private System.Windows.Forms.ToolStripButton originalSizeButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnUp;
        private System.Windows.Forms.ToolStripButton btnDown;
    }
}


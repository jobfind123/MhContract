﻿namespace Sbk_Manage
{
    partial class Frm_Htjy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Htjy));
            this.txtJyr = new System.Windows.Forms.TextBox();
            this.txtHtmc = new System.Windows.Forms.TextBox();
            this.txtHtbh = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpHtJcrq = new System.Windows.Forms.DateTimePicker();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtJyr
            // 
            this.txtJyr.Location = new System.Drawing.Point(91, 193);
            this.txtJyr.MaxLength = 100;
            this.txtJyr.Name = "txtJyr";
            this.txtJyr.Size = new System.Drawing.Size(191, 26);
            this.txtJyr.TabIndex = 9;
            // 
            // txtHtmc
            // 
            this.txtHtmc.Location = new System.Drawing.Point(91, 162);
            this.txtHtmc.MaxLength = 100;
            this.txtHtmc.Name = "txtHtmc";
            this.txtHtmc.ReadOnly = true;
            this.txtHtmc.Size = new System.Drawing.Size(191, 26);
            this.txtHtmc.TabIndex = 8;
            this.txtHtmc.TabStop = false;
            // 
            // txtHtbh
            // 
            this.txtHtbh.Location = new System.Drawing.Point(91, 131);
            this.txtHtbh.MaxLength = 100;
            this.txtHtbh.Name = "txtHtbh";
            this.txtHtbh.ReadOnly = true;
            this.txtHtbh.Size = new System.Drawing.Size(191, 26);
            this.txtHtbh.TabIndex = 7;
            this.txtHtbh.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(7, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "借 阅 人：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(7, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "合同名称：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(7, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "合同编号：";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("楷体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(29, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(253, 99);
            this.label4.TabIndex = 10;
            this.label4.Text = "麦禾合同管理系统";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(5, 265);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 41);
            this.button1.TabIndex = 11;
            this.button1.Text = "合同借出";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(108, 265);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 41);
            this.button2.TabIndex = 11;
            this.button2.Text = "合同归还";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(7, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "借出时间：";
            // 
            // dtpHtJcrq
            // 
            this.dtpHtJcrq.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpHtJcrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpHtJcrq.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHtJcrq.Location = new System.Drawing.Point(91, 224);
            this.dtpHtJcrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHtJcrq.Name = "dtpHtJcrq";
            this.dtpHtJcrq.Size = new System.Drawing.Size(191, 26);
            this.dtpHtJcrq.TabIndex = 12;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(211, 265);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 41);
            this.button3.TabIndex = 13;
            this.button3.Text = "借出单打印";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Frm_Htjy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImage = global::Sbk_Manage.Properties.Resources.HtGL;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(452, 321);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dtpHtJcrq);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtJyr);
            this.Controls.Add(this.txtHtmc);
            this.Controls.Add(this.txtHtbh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Htjy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "合同借阅";
            this.Load += new System.EventHandler(this.Frm_Htjy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtJyr;
        private System.Windows.Forms.TextBox txtHtmc;
        private System.Windows.Forms.TextBox txtHtbh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.DateTimePicker dtpHtJcrq;
        private System.Windows.Forms.Button button3;
    }
}